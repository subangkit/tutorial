import React,{Component} from 'react';
import Produk from './Produk/Produk';

// parent to child (produk)

class App extends Component{
// pembuatan state
state = {
  produk :[
    {id:1,namaProduk:'LCD',harga:1000000,stok:5},
    {id:2,namaProduk:'Monitor',harga:900000,stok:5},
    {id:3,namaProduk:'Printer',harga:80000,stok:5},
    {id:4,namaProduk:'Proyektor',harga:560000,stok:5},
    {id:5,namaProduk:'Laptop',harga:7000000,stok:5},
  ]
}


  render(){

    return(
      <div>
        <p>Belajar Jam Pertama</p>
        <Produk produkProduk={this.state.produk}/>

      </div>
    );
  }
}

export default App;
