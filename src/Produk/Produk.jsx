import React,{Component} from 'react'
import './Produk.css';

class Produk extends Component{
    render(){
        // const {namaProduk,harga,stok} = this.props //ini jika menghilangkan embel embel didepanya 
        // console.log(this.props)
        // this.props.namaProduk => pemanggilan default dari APP js

        const {produkProduk} = this.props;
        // cetak
        const listProduk = produkProduk.map(produk => {
            return(
                <div className="card-produk" key={produk.id}>
                        <p className="produk-font">Nama produk : {produk.namaProduk}</p>
                        <p className="produk-font">Harga : {produk.harga}</p>
                        <p className="produk-font">Stok :{produk.stok}</p>
                </div>
            )
        })//end map

        return(
            <div>
                {/* cetak listProduk disini */}
                {listProduk}
            </div>    
        )
    }
}

export default Produk;
